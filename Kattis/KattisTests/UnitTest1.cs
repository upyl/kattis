﻿using Kattis;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace KattisTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            InternalTest("in1.txt", "out1.txt");
        }

        private static void InternalTest(string inf, string outf)
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                using (StringReader sr = new StringReader(File.ReadAllText(inf)))
                {
                    Console.SetIn(sr);

                    Program.Main(new string[] { });

                    string expected = File.ReadAllText(outf);
                    Assert.AreEqual(expected, sw.ToString());
                }
            }
        }
    }
}