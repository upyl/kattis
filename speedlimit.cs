using System;

namespace SpeedLimit
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string input;
            while ((input = Console.ReadLine()) != "-1")
            {
                var currentResult = 0;
                var previoustime = 0;
                for (var i = 0; i < int.Parse(input); i++)
                {
                    var step = Console.ReadLine().Split(' ');
                    var speed = int.Parse(step[0]);
                    var time = int.Parse(step[1]);
                    currentResult += speed * (time - previoustime);
                    previoustime = time;
                }
                Console.WriteLine(currentResult + " miles");
            }
        }
    }
}