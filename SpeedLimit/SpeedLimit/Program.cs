﻿using System;

namespace SpeedLimit
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var t = Console.ReadLine().Split(' ');
            var h = int.Parse(t[0]);
            var v = int.Parse(t[1]);
            Console.WriteLine(Math.Ceiling( h / Math.Sin(v * Math.PI / 180)));
        }
    }
}